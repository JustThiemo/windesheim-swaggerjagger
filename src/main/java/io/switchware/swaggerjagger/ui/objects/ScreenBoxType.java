package io.switchware.swaggerjagger.ui.objects;

import io.switchware.swaggerjagger.ui.draw.ColorDrawable;
import io.switchware.swaggerjagger.ui.draw.Drawable;
import javafx.scene.paint.Color;
import lombok.Getter;

public enum ScreenBoxType {

	COLOR_WHITE ( new ColorDrawable ( Color.WHITE ) ),
	COLOR_GRAY ( new ColorDrawable ( Color.LIGHTGRAY ) ),
	COLOR_RED ( new ColorDrawable ( Color.RED ) ),
	COLOR_ORANGE ( new ColorDrawable ( Color.ORANGE ) ),
	COLOR_YELLOW ( new ColorDrawable ( Color.YELLOW ) ),
	COLOR_GREEN ( new ColorDrawable ( Color.GREEN ) ),
	COLOR_BLUE ( new ColorDrawable ( Color.BLUE ) ),
	COLOR_PURPLE ( new ColorDrawable ( Color.MAGENTA ) ),
	COLOR_BLACK ( new ColorDrawable ( Color.BLACK ) );

	public static final   ScreenBoxType[] CACHE = values ();
	@Getter private final Drawable        drawable;

	ScreenBoxType ( Drawable drawable ) { this.drawable = drawable; }

	public static ScreenBoxType fromId ( int id ) {

		for ( ScreenBoxType type : CACHE ) if ( type.ordinal () == id ) return type;
		throw new IllegalArgumentException ( "Invalid ScreenBoxType ID " + id );
	}

	public static ScreenBoxType getNext ( ScreenBoxType current ) { return fromId ( ( current.ordinal () + 1 ) % CACHE.length ); }
}
