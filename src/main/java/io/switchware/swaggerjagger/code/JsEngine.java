package io.switchware.swaggerjagger.code;

import io.switchware.swaggerjagger.Main;
import io.switchware.swaggerjagger.balance.CostHandler;
import io.switchware.swaggerjagger.balance.CostType;
import io.switchware.swaggerjagger.keyword.KeyWordException;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.HashMap;

public class JsEngine {

	private static final HashMap<String, CostType> OPERATORS = new HashMap<> ();

	static {
		OPERATORS.put ( "+", CostType.OPERATION );
		OPERATORS.put ( "-", CostType.OPERATION );
		OPERATORS.put ( "*", CostType.OPERATION );
		OPERATORS.put ( "%", CostType.OPERATION );
		OPERATORS.put ( "/", CostType.OPERATION );

		OPERATORS.put ( "==", CostType.COMPARE );
		OPERATORS.put ( "<", CostType.COMPARE );
		OPERATORS.put ( ">", CostType.COMPARE );
		OPERATORS.put ( "!=", CostType.COMPARE );
	}

	public static boolean eval ( String line ) throws KeyWordException {

		line = replaceLine ( line );

		ScriptEngineManager mgr    = new ScriptEngineManager ();
		ScriptEngine        engine = mgr.getEngineByName ( "JavaScript" );
		try { return ( boolean ) engine.eval ( line ); } catch ( ScriptException e ) { e.printStackTrace (); }
		throw new KeyWordException ( "Invalid operation: " + line );
	}

	public static int evalInt ( String line ) throws KeyWordException {

		line = replaceLine ( line );

		ScriptEngineManager mgr    = new ScriptEngineManager ();
		ScriptEngine        engine = mgr.getEngineByName ( "JavaScript" );
		try { return ( int ) engine.eval ( line ); } catch ( ScriptException e ) { e.printStackTrace (); }
		throw new KeyWordException ( "Invalid operation: " + line );
	}

	private static String replaceLine ( String line ) throws KeyWordException {

		for ( char letter : Main.getInstance ().getCodeExecutor ().getKeyWordHandler ().getVariables () ) {
			if ( Main.getInstance ().getCodeExecutor ().isVariableAssigned ( letter ) ) {
				line = line.replace ( String.valueOf ( letter ), String.valueOf ( Main.getInstance ().getCodeExecutor ().getIntValue ( String.valueOf ( letter ) ) ) );
			}
		}

		// Do all costs for this math formula.
		for ( String c : line.split ( " " ) ) {
			if(OPERATORS.containsKey ( c )) {
				CostHandler.getInstance ().takeCosts ( OPERATORS.get ( c ) );
			}
		}

		return line;
	}
}
