package io.switchware.swaggerjagger.ui.draw;

import io.switchware.swaggerjagger.Main;
import io.switchware.swaggerjagger.ui.objects.ScreenObject;
import io.switchware.swaggerjagger.ui.objects.ScreenObjectType;
import io.switchware.swaggerjagger.ui.objects.ScreenPart;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import static io.switchware.swaggerjagger.Main.MAZE_BOX_WIDTH;

public class ImageDrawable implements Drawable {

	private final Image img;

	public ImageDrawable ( final String image ) { img = Main.loadImage ( image ); }

	@Override public void onDraw ( final GraphicsContext graphics, final ScreenPart box ) {

		int x = box.getX () * MAZE_BOX_WIDTH, y = box.getY () * MAZE_BOX_WIDTH;

		if ( ( ( ScreenObject ) box ).getType ().getBackground () != null ) {
			graphics.setFill ( ( ( ScreenObject ) box ).getType ().getBackground () );
			graphics.fillRect ( x, y, x + MAZE_BOX_WIDTH, y + MAZE_BOX_WIDTH );
		}

		if ( img == null ) {
			graphics.setFill ( Color.RED );
			graphics.fillRect ( x + ( MAZE_BOX_WIDTH >> 2 ), y + ( MAZE_BOX_WIDTH >> 2 ), MAZE_BOX_WIDTH >> 1, MAZE_BOX_WIDTH >> 1 );
		} else graphics.drawImage ( img, x, y, MAZE_BOX_WIDTH, MAZE_BOX_WIDTH );

		int textX = x + 2, textY = y + MAZE_BOX_WIDTH - 2;

		switch ( ( ( ScreenObject ) box ).getType () ) {
			case BOMB -> {
				graphics.setFill ( Color.WHITE );
				graphics.fillText ( ( ( ScreenObject ) box ).getValue () + "s", textX, textY );
			}
			case TARGET -> {
				graphics.setFill ( Color.BLACK );
				graphics.fillText ( String.valueOf ( ( ( ScreenObject ) box ).getValue () ), textX, textY );
			}
			case TURN -> {
				graphics.setFill ( Color.YELLOW );
				graphics.fillText ( String.valueOf ( Math.max ( 0, Math.min ( ( ( ScreenObject ) box ).getValue (), 3 ) ) ), textX, textY );
			}
			case BONUS -> {
				graphics.setFill ( Color.BLACK );
				graphics.fillText ( "€" + Math.pow ( 2, ( ( ScreenObject ) box ).getValue () ), textX, textY );
			}
		}
	}
}
