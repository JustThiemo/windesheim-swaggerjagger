package io.switchware.swaggerjagger.code;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;

@Getter @AllArgsConstructor public class CodeBlock {

	private int startLine, endLine;
	private final ArrayList < String > linesInside = new ArrayList <> ();

	@Override public String toString () { return "CodeBlock: " + startLine + ", " + endLine + ", " + linesInside.size () + " lines"; }
}
