package io.switchware.swaggerjagger.ui;

import io.switchware.swaggerjagger.balance.CostType;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import static io.switchware.swaggerjagger.Main.SPACING;

public class CostWindow {

	private static final CostWindow INSTANCE = new CostWindow ();

	private final Stage       stage;
	private       TextField[] fields = new TextField[ CostType.CACHE.length ];

	private CostWindow () {

		stage = new Stage ();
		stage.setTitle ( "Cost Editor" );
		stage.getIcons ().setAll ( GameWindow.getIcon () );

		BorderPane root = new BorderPane ();
		GridPane   grid = new GridPane ();
		grid.setPadding ( new Insets ( SPACING ) );
		grid.setHgap ( SPACING );
		grid.setVgap ( SPACING );

		{ // Header
			grid.add ( new Label ( "Hardware (aanschaf)" ), 0, 0 );
			grid.add ( new Label ( "Kosten" ), 1, 0 );
			grid.add ( new Label ( "Verbruik" ), 2, 0 );
			grid.add ( new Label ( "Kosten" ), 3, 0 );
			grid.add ( new Label ( "Software (per geschreven statement" ), 4, 0 );
			grid.add ( new Label ( "Kosten" ), 5, 0 );
		}

		for ( final CostType type : CostType.CACHE ) {
			grid.add ( new Label ( type.getName () ), type.getX () * 2, type.getY () );
			grid.add ( fields[ type.ordinal () ] = new TextField ( String.valueOf ( type.getCost () ) ), type.getX () * 2 + 1, type.getY () );
			fields[ type.ordinal () ].textProperty ().addListener ( ( o, ov, nv ) -> {
				try {
					type.setCost ( Integer.parseInt ( fields[ type.ordinal () ].getText () ) );
				} catch ( final NumberFormatException e ) {
					System.err.println ( e.getLocalizedMessage () );
				}
			} );
		}

		root.setCenter ( grid );
		stage.setScene ( new Scene ( root, 500, 400 ) );
	}

	public static CostWindow getInstance () { return INSTANCE; }

	public void show () { stage.show (); }
}
