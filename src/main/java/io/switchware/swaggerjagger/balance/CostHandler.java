package io.switchware.swaggerjagger.balance;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter public class CostHandler {

	private static CostHandler INSTANCE;

	private int balance          = 2020;
	private int totalDeclareCost = 0;

	public void takeCosts ( CostType type ) {

		if ( type.name ().startsWith ( "DECLARE_" ) ) totalDeclareCost += type.getCost ();
		else System.out.println ( "Kosten voor " + type.getName () + ": " + type.getCost () );
		balance -= type.getCost ();
	}

	public int getCosts ( CostType type ) { return type.getCost (); }

	public static CostHandler getInstance () {

		if ( INSTANCE == null ) INSTANCE = new CostHandler ();
		return INSTANCE;
	}

	public void addBonus ( final int bonus ) {

		System.out.println ( "Bonus van " + bonus + " opgepakt" );
		balance += bonus;
	}
}
