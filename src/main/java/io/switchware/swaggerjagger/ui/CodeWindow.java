package io.switchware.swaggerjagger.ui;

import io.switchware.swaggerjagger.ui.objects.ScreenObject;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public final class CodeWindow {

	private static final CodeWindow INSTANCE = new CodeWindow ();

	private final Stage    stage;
	private final TextArea area;

	private CodeWindow () {

		stage = new Stage ();
		stage.setTitle ( "20 Code Editor" );
		stage.getIcons ().setAll ( GameWindow.getIcon () );

		BorderPane root = new BorderPane ();
		area = new TextArea ();
		area.setFont ( Font.font ( java.awt.Font.MONOSPACED, 14 ) );

		root.setCenter ( area );
		stage.setScene ( new Scene ( root, 400, 280 ) );
	}

	public static CodeWindow getInstance () { return INSTANCE; }

	public void show () {

		stage.show ();
		stage.requestFocus ();
	}

	public String getText () { return area.getText (); }
}
