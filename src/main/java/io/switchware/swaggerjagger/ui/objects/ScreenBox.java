package io.switchware.swaggerjagger.ui.objects;

public class ScreenBox extends ScreenPart < ScreenBoxType > {

	public ScreenBox ( int x, int y ) { super ( ScreenBoxType.COLOR_WHITE, x, y ); }

	@Override public boolean equals ( Object obj ) { return obj instanceof ScreenBox && ( x == ( ( ScreenBox ) obj ).x && y == ( ( ScreenBox ) obj ).y ); }

	@Override public void setTypeNext () { this.type = ScreenBoxType.getNext ( this.type ); }
}
