package io.switchware.swaggerjagger.keyword.types;

import io.switchware.swaggerjagger.Main;
import io.switchware.swaggerjagger.balance.CostHandler;
import io.switchware.swaggerjagger.balance.CostType;
import io.switchware.swaggerjagger.code.CodeBlock;
import io.switchware.swaggerjagger.code.CodeHardware;
import io.switchware.swaggerjagger.code.JsEngine;
import io.switchware.swaggerjagger.keyword.KeyWordException;
import io.switchware.swaggerjagger.keyword.KeyWordHandler;
import io.switchware.swaggerjagger.ui.GameWindow;

import java.util.concurrent.TimeUnit;

public class ZolangKeyHandler extends KeyWordHandler {

	public static boolean FORCE_STOP = false;

	@Override public void onHandle ( int lineIndex, String args, CodeBlock block ) throws KeyWordException {

		if ( args.isEmpty () ) throw new KeyWordException ( "Empty zolang statement" );
		long     until = System.currentTimeMillis () + TimeUnit.SECONDS.toMillis ( 60 );
		String[] parts = args.replace ( " {", "" ).replace ( "{", "" ).split ( " " );

		switch ( parts[ 0 ].toLowerCase () ) {
			case "kompas" -> {
				if ( !Main.getInstance ().getCodeExecutor ().hasHardware ( CodeHardware.COMPASS ) ) throw new KeyWordException ( "Can not use kompas when it is not initialized" );

				while ( !FORCE_STOP ) {
					String  line       = args.trim ().replace ( "{", "" ).replace ( "kompas", GameWindow.getInstance ().getPoppetje ().kompas () + "" );
					boolean stillGoing = JsEngine.eval ( line );
					if ( !stillGoing ) break;

					check ( until );
					Main.getInstance ().getCodeExecutor ().executeCodeBlock ( block );
				}
			}
			case "kleuroog" -> {
				Main.log ( "Kleuroog executed..." );
				if ( !Main.getInstance ().getCodeExecutor ().hasHardware ( CodeHardware.COLOR_EYE ) ) throw new KeyWordException ( "Can not use kleuroog when it is not initialized" );

				while ( !FORCE_STOP ) {
					String  line       = args.trim ().replace ( "{", "" ).replace ( "kleuroog", GameWindow.getInstance ().getPoppetje ().kleurOog () + "" );
					boolean stillGoing = JsEngine.eval ( line );
					Main.log ( "Looking for: " + Main.getInstance ().getCodeExecutor ().getIntValue ( parts[ 2 ] ) );
					Main.log ( "Color is " + GameWindow.getInstance ().getPoppetje ().kleurOog () );

					if ( !stillGoing ) break;
					check ( until );
					Main.getInstance ().getCodeExecutor ().executeCodeBlock ( block );
				}
			}
			case "zwoog" -> {
				if ( !Main.getInstance ().getCodeExecutor ().hasHardware ( CodeHardware.BW_EYE ) ) throw new KeyWordException ( "Can not use zwoog when it is not initialized" );

				while ( !FORCE_STOP ) {
					String  line       = args.trim ().replace ( "{", "" ).replace ( "zwoog", GameWindow.getInstance ().getPoppetje ().zwOog () + "" );
					boolean stillGoing = JsEngine.eval ( line );
					if ( !stillGoing ) break;

					check ( until );
					Main.getInstance ().getCodeExecutor ().executeCodeBlock ( block );
				}
			}
			default -> {
				while ( !FORCE_STOP ) {
					String  line       = args.trim ().replace ( "{", "" );
					boolean stillGoing = JsEngine.eval ( line );
					if ( !stillGoing ) break;

					check ( until );
					Main.getInstance ().getCodeExecutor ().executeCodeBlock ( block );
				}
			}
		}
	}

	void check ( long until ) throws KeyWordException { if ( System.currentTimeMillis () >= until ) throw new KeyWordException ( "Your while loop took way too long buddy, most likely your code just sucks, oops." ); }

	@Override public boolean isBlock () { return true; }
}
