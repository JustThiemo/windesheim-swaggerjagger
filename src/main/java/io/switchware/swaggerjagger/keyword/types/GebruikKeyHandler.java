package io.switchware.swaggerjagger.keyword.types;

import io.switchware.swaggerjagger.Main;
import io.switchware.swaggerjagger.balance.CostHandler;
import io.switchware.swaggerjagger.balance.CostType;
import io.switchware.swaggerjagger.code.CodeBlock;
import io.switchware.swaggerjagger.code.CodeHardware;
import io.switchware.swaggerjagger.keyword.KeyWordException;
import io.switchware.swaggerjagger.keyword.KeyWordHandler;

import java.util.ArrayList;
import java.util.List;

public class GebruikKeyHandler extends KeyWordHandler {

	@Override public void onHandle ( int lineIndex, String args, CodeBlock block ) throws KeyWordException {

		if ( args.isEmpty () ) throw new KeyWordException ( "Empty gebruik statement" );
		if ( !Main.getInstance ().getCodeExecutor ().canUse () ) throw new KeyWordException ( "Can not use after making code!" );

		// Must be a letter from A-Z.
		switch ( args.toLowerCase () ) {
			case "kompas" -> {
				Main.log ( "Enabled compas" );
				Main.getInstance ().getCodeExecutor ().addHardware ( CodeHardware.COMPASS );
			}
			case "kleuroog" -> {
				Main.log ( "Enabled kleuroog" );
				Main.getInstance ().getCodeExecutor ().addHardware ( CodeHardware.COLOR_EYE );
			}
			case "zwoog" -> {
				Main.log ( "Enabled zwoog" );
				Main.getInstance ().getCodeExecutor ().addHardware ( CodeHardware.BW_EYE );
			}
			default -> {
				if ( args.length () > 1 ) throw new KeyWordException ( "Can not assign variable with more than 1 letter. Please use A-Z" );

				List<Character> chars = new ArrayList<> ();
				for ( char c : Main.getInstance ().getCodeExecutor ().getKeyWordHandler ().getVariables () ) chars.add ( c );
				char c = args.charAt ( 0 );

				if ( !chars.contains ( c ) ) throw new KeyWordException ( c + " is not a valid character used A-Z" );
				if ( Main.getInstance ().getCodeExecutor ().isVariableAssigned ( c ) ) throw new KeyWordException ( c + " is already assigned as a variable." );

				Main.getInstance ().getCodeExecutor ().assignVariable ( c );
				Main.log ( "Assigned variable " + c );
				CostHandler.getInstance ().takeCosts ( CostType.VARIALBE );
				break;
			}

		}
	}

	@Override public boolean isBlock () {

		return false;
	}
}
