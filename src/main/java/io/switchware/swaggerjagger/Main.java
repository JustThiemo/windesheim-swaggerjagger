package io.switchware.swaggerjagger;

import io.switchware.swaggerjagger.code.CodeExecutor;
import io.switchware.swaggerjagger.keyword.KeyWordException;
import io.switchware.swaggerjagger.keyword.KeyWords;
import io.switchware.swaggerjagger.ui.CodeWindow;
import io.switchware.swaggerjagger.ui.GameWindow;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

	public static final String TITLE     = "(づ｡◕‿‿◕｡)づ | SwaggerJagger ~ SwitchWare.io";
	public static final int    SPACING   = 8;
	public static final int    MAZE_SIZE = 800, MAZE_BOXES = 20, MAZE_BOX_WIDTH = MAZE_SIZE / MAZE_BOXES;
	public static final File GLADE_FILE = new File ( "./glade.dat" );

	private static Main    instance;
	private static boolean DEBUG = false;

	private final CodeExecutor codeExecutor;
	private       boolean      busy = false;

	public static void main ( String[] args ) { new Main (); }

	private Main () {

		instance = this;
		this.codeExecutor = new CodeExecutor ( new KeyWords () ); // Initialize code executor.
		codeExecutor.getKeyWordHandler ().load ();
		Application.launch ( GameWindow.class, "" );
	}

	public static Main getInstance () { return instance; }

	public void run () {

		if ( busy ) return;
		busy = true;
		try {
			List < String > lines = new ArrayList <> ( Arrays.asList ( CodeWindow.getInstance ().getText ().split ( "\n" ) ) );
			Main.log ( "Loaded " + lines.size () + " lines of 'code', executing this right now..." );
			// A little delay after clicking button would be nice right.
			Thread.sleep ( 500 );

			new Thread ( () -> {
				codeExecutor.clear ();
				try {
					codeExecutor.preAnalyze ( lines );
				} catch ( KeyWordException ex ) {
					ex.printStackTrace ();
					showError ( ex );
				} finally { busy = false; }
			} ).start ();
		} catch ( InterruptedException ex ) {
			ex.printStackTrace ();
			busy = false;
		}
	}

	public static void showError ( final Exception e ) {

		Platform.runLater ( () -> {
			Alert alert = new Alert ( Alert.AlertType.ERROR );
			alert.setTitle ( "Oh tyfus tering! Een fout..." );
			alert.setHeaderText ( "Well, you got an error retard!" );
			alert.setContentText ( e.getLocalizedMessage () );
			( ( Stage ) alert.getDialogPane ().getScene ().getWindow () ).getIcons ().setAll ( GameWindow.getIcon () );

			// Create expandable Exception.
			StringWriter sw = new StringWriter ();
			PrintWriter  pw = new PrintWriter ( sw );
			e.printStackTrace ( pw );
			String exceptionText = sw.toString ();

			TextArea textArea = new TextArea ( exceptionText );
			textArea.setEditable ( false );
			textArea.setWrapText ( true );

			textArea.setMaxWidth ( Double.MAX_VALUE );
			textArea.setMaxHeight ( Double.MAX_VALUE );

			alert.getDialogPane ().setExpandableContent ( textArea );
			alert.showAndWait ();
		} );
	}

	public static Image loadImage ( final String image ) {

		try {
			return new Image ( image );
		} catch ( IllegalArgumentException e ) {
			Main.showError ( e );
			return null;
		}
	}

	public boolean toggleDebug () { return DEBUG = !DEBUG; }

	public CodeExecutor getCodeExecutor () { return codeExecutor; }

	public static void log ( String log ) { if ( DEBUG ) System.out.println ( log ); }
}
