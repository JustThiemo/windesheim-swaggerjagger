package io.switchware.swaggerjagger.balance;

import lombok.Getter;
import lombok.Setter;

@Getter public enum CostType {

	// HARDWARE
	COMPAS ( 10, "kompas", 0, 1 ),
	ZW_OOG ( 3, "zwoog", 0, 2 ),
	KLEUR_OOG ( 15, "kleuroog", 0, 3 ),
	VARIALBE ( 1, "variabele", 0, 4 ),

	// USAGE
	STEP_FORWARD ( 2, "stapVooruit", 1, 1 ),
	STEP_BACKWARDS ( 3, "stapAchteruit", 1, 2 ),
	TURN_LEFT ( 5, "draaiLinks", 1, 3 ),
	TURN_RIGHT ( 5, "draaiRechts", 1, 4 ),
	USE_ZW_OOG ( 1, "use_zwoog", 1, 5 ),
	USE_KLEUR_OOG ( 1, "use_kleuroog", 1, 6 ),
	USE_COMPAS ( 5, "use_compas", 1, 7 ),
	BUMP ( 650, "duw", 1, 8 ),
	USE_ASSIGNMENT ( 1, "toewijzing", 1, 9 ),
	OPERATION ( 2, "toewijzing", 1, 10 ),
	COMPARE ( 1, "vergelijking", 1, 11 ),

	// STATEMENTS
	DECLARE_WHILE ( 4, "zolang", 2, 1 ),
	DECLARE_IF ( 4, "als", 2, 2 ),
	DECLARE_EXERCISE ( 2, "opdracht", 2, 3 ),
	DECLARE_ASSISNGMENT ( 2, "toekenning", 2, 4 );

	public static final     CostType[] CACHE = CostType.values ();
	@Setter @Getter private int        cost;
	private                 String     name;
	@Getter private final   int        x, y;

	CostType ( int defaultCost, String name, int x, int y ) {

		this.x = x;
		this.y = y;
		this.cost = defaultCost;
		this.name = name;
	}
}
