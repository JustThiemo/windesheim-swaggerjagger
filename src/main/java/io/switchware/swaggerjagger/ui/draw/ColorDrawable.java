package io.switchware.swaggerjagger.ui.draw;

import io.switchware.swaggerjagger.Main;
import io.switchware.swaggerjagger.ui.objects.ScreenPart;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Paint;

public class ColorDrawable implements Drawable {

	private final Paint paint;

	public ColorDrawable ( Paint paint ) { this.paint = paint; }

	@Override public void onDraw ( final GraphicsContext graphics, final ScreenPart box ) {

		final int x = box.getX () * Main.MAZE_BOX_WIDTH, y = box.getY () * Main.MAZE_BOX_WIDTH;

		graphics.setFill ( paint );
		graphics.fillRect ( x, y, Main.MAZE_BOX_WIDTH, Main.MAZE_BOX_WIDTH );
	}
}
