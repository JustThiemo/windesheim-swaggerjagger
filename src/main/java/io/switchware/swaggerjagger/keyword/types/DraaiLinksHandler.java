package io.switchware.swaggerjagger.keyword.types;

import io.switchware.swaggerjagger.balance.CostHandler;
import io.switchware.swaggerjagger.balance.CostType;
import io.switchware.swaggerjagger.code.CodeBlock;
import io.switchware.swaggerjagger.keyword.KeyWordException;
import io.switchware.swaggerjagger.keyword.KeyWordHandler;
import io.switchware.swaggerjagger.ui.GameWindow;

public class DraaiLinksHandler extends KeyWordHandler {

	@Override public void onHandle ( int lineIndex, String args, CodeBlock block ) throws KeyWordException {

		GameWindow.getInstance ().getPoppetje ().turnLeft ();
		CostHandler.getInstance ().takeCosts ( CostType.TURN_LEFT );
		waitPls ();
	}

	@Override public boolean isBlock () { return false; }
}
