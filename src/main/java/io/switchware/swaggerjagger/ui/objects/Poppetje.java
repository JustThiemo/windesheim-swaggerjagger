package io.switchware.swaggerjagger.ui.objects;

import io.switchware.swaggerjagger.Main;
import io.switchware.swaggerjagger.balance.CostHandler;
import io.switchware.swaggerjagger.balance.CostType;
import io.switchware.swaggerjagger.ui.GameWindow;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.util.Random;

import static io.switchware.swaggerjagger.Main.MAZE_BOX_WIDTH;
import static java.lang.Math.max;
import static java.lang.Math.min;

enum Direction {

	NORTH,
	EAST,
	SOUTH,
	WEST;

	public static final Direction[] CACHE = values ();

	public static Direction getNext ( final Direction current ) {

		return fromId ( ( current.ordinal () + 1 ) % CACHE.length );
	}

	public static Direction getPrevious ( final Direction current ) {

		return fromId ( ( current.ordinal () == 0 ? CACHE.length : current.ordinal () ) - 1 );
	}

	public static Direction fromId ( int id ) {

		for ( Direction type : CACHE ) if ( type.ordinal () == id ) return type;
		throw new IllegalArgumentException ( "Invalid Direction ID " + id );
	}
}

class Position {

	int x, y;

	Position ( int x, int y ) {

		this.x = x;
		this.y = y;
	}

	static Position next ( Position base, Direction direction ) {

		int x = base.x, y = base.y;

		switch ( direction ) {
			case NORTH -> y--;
			case EAST -> x++;
			case SOUTH -> y++;
			case WEST -> x--;
		}
		x = clamp ( x );
		y = clamp ( y );

		return new Position ( x, y );
	}

	static Position previous ( Position base, Direction direction ) {

		int x = base.x, y = base.y;

		switch ( direction ) {
			case NORTH -> y++;
			case EAST -> x--;
			case SOUTH -> y--;
			case WEST -> x++;
		}
		x = clamp ( x );
		y = clamp ( y );

		return new Position ( x, y );
	}

	private static int clamp ( int value ) {

		return max ( 0, min ( value, 19 ) );
	}
}

public class Poppetje {

	private static final Image img;

	static {
		img = Main.loadImage ( "/poppetje.png" );
	}

	private Direction direction = Direction.SOUTH;
	private Position  position  = new Position ( 5, 7 );

	public void draw ( final GraphicsContext graphics ) {

		int x = position.x * MAZE_BOX_WIDTH, y = position.y * MAZE_BOX_WIDTH;
		graphics.drawImage ( img, x, y, MAZE_BOX_WIDTH, MAZE_BOX_WIDTH );

		int textX = x + 2, textY = y + MAZE_BOX_WIDTH - 2;
		graphics.setFill ( Color.RED );
		graphics.fillText ( String.valueOf ( direction.ordinal () ), textX, textY );
	}

	public void turnRight () {

		direction = Direction.getNext ( direction );
		GameWindow.getInstance ().redrawMaze ();
	}

	public void turnLeft () {

		direction = Direction.getPrevious ( direction );
		GameWindow.getInstance ().redrawMaze ();
	}

	public void moveForward () {

		Position next = Position.next ( this.position, direction );
		doMovement ( next );
	}

	public void moveBackwards () {

		Position next = Position.previous ( this.position, direction );
		doMovement ( next );
	}

	private void doMovement ( Position next ) {

		final ScreenObject obj = GameWindow.getInstance ().getObjectAt ( next.x, next.y );
		switch ( obj.getType () ) {
			case BOMB -> {
				// Fock die bom die boeit echt niks.
			}
			case OBSTACLE, TREE, BUSH -> {
				CostHandler.getInstance ().takeCosts ( CostType.BUMP );
				GameWindow.getInstance ().redrawMaze ();
				return;
			}
			case TARGET -> {
				Main.getInstance ().getCodeExecutor ().offerTarget ( obj.getValue () );
				System.out.println ( String.format ( "Doel %s bereikt [%d,%d]", obj.getValue (), obj.y, obj.x ) );
			}
			case TURN -> {
				int type = Math.max ( 0, Math.min ( obj.getValue (), 3 ) );
				makeTurns ( type == 0 ? new Random ().nextInt ( 5 ) : type );
			}
			case BONUS -> CostHandler.getInstance ().addBonus ( ( int ) Math.pow ( 2, ( ( ScreenObject ) obj ).getValue () ) );
		}

		this.position = next;
		GameWindow.getInstance ().redrawMaze ();
	}

	private void makeTurns ( int amount ) { for ( int i = 0; i < amount; i++ ) turnRight (); }

	public int kompas () {

		CostHandler.getInstance ().takeCosts ( CostType.USE_COMPAS );
		return switch ( direction ) {
			case NORTH -> 0;
			case EAST -> 1;
			case SOUTH -> 2;
			case WEST -> 3;
		};
	}

	public int kleurOog () {

		CostHandler.getInstance ().takeCosts ( CostType.USE_KLEUR_OOG );
		return switch ( GameWindow.getInstance ().getBoxAt ( position.x, position.y ).getType () ) {
			case COLOR_WHITE -> 8;
			case COLOR_GRAY -> 7;
			case COLOR_RED -> 6;
			case COLOR_ORANGE -> 5;
			case COLOR_YELLOW -> 4;
			case COLOR_GREEN -> 3;
			case COLOR_BLUE -> 2;
			case COLOR_PURPLE -> 1;
			case COLOR_BLACK -> 0;
		};
	}

	public int zwOog () {

		CostHandler.getInstance ().takeCosts ( CostType.USE_ZW_OOG );
		return GameWindow.getInstance ().getBoxAt ( position.x, position.y ).getType ().equals ( ScreenBoxType.COLOR_BLACK ) ? 0 : 1;
	}

	public void reset () {

		CostHandler.getInstance ().setBalance ( 2020 );
		CostHandler.getInstance ().setTotalDeclareCost ( 0 );

		position = new Position ( 5, 7 );
		direction = Direction.SOUTH;
		GameWindow.getInstance ().redrawMaze ();
	}

	public int getX () { return position.x; }

	public int getY () { return position.y; }
}
