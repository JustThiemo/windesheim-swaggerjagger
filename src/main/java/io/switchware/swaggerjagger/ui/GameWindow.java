package io.switchware.swaggerjagger.ui;

import io.switchware.swaggerjagger.Main;
import io.switchware.swaggerjagger.ui.objects.*;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import lombok.Getter;

import java.io.*;

import static io.switchware.swaggerjagger.Main.*;
import static javafx.geometry.Pos.BASELINE_CENTER;

public class GameWindow extends Application {

	private static GameWindow instance;
	private static Image      icon;

	private         Stage            stage;
	private         GraphicsContext  graphics;
	private         ScreenBox[][]    boxes    = new ScreenBox[ MAZE_BOXES ][ MAZE_BOXES ];
	@Getter private ScreenObject[][] objects  = new ScreenObject[ MAZE_BOXES ][ MAZE_BOXES ];
	private         Poppetje         poppetje = new Poppetje ();

	private BorderPane root = new BorderPane (), game = new BorderPane ();
	private HBox   sysButtons;
	private Button btnCode, btnStart, btnRun, btnReset, btnDebug;
	private ComboBox < ScreenBoxType > boxTypes;

	public GameWindow () {

		if ( instance != null ) throw new IllegalStateException ( "GameWindow already initialized!" );
		instance = this;
	}

	@Override public void start ( final Stage stage ) {

		( this.stage = stage ).setTitle ( TITLE );
		stage.getIcons ().setAll ( icon = Main.loadImage ( "/20.png" ) );
		stage.setResizable ( false );

		sysButtons = createHBox ();
		sysButtons.getChildren ().add ( btnCode = new Button ( "Lets Code!" ) );
		sysButtons.getChildren ().add ( btnStart = new Button ( "Start" ) );
		sysButtons.getChildren ().add ( btnRun = new Button ( "Lets Run!" ) );
		btnReset = new Button ( "Reset" );
		sysButtons.getChildren ().add ( btnDebug = new Button ( "Enable Debug" ) );

		final Canvas maze = new Canvas ( MAZE_SIZE, MAZE_SIZE );
		graphics = maze.getGraphicsContext2D ();
		maze.setOnMouseClicked ( this::onMazeEdit );

		final VBox welcomeText = createVBox ();
		welcomeText.getChildren ().add ( new Label ( TITLE ) );
		welcomeText.getChildren ().add ( new Label ( "Welcome to the most shit and rushed coding project of all the existing projects." ) );
		welcomeText.getChildren ().add ( new Label ( "Please press the \'Lets Code!\' button to start coding your maze code," ) );
		welcomeText.getChildren ().add ( new Label ( "which technically is just a weird rip-off code in Dutch, but oh well ʕ•ᴥ•ʔ" ) );

		final HBox gameButtons = createHBox ();
		Button     btnStapVooruit, btnStapAchteruit, btnDraaiLinks, btnDraaiRechts, btnCosts;
		gameButtons.getChildren ().add ( btnStapVooruit = new Button ( "stapVooruit" ) );
		gameButtons.getChildren ().add ( btnStapAchteruit = new Button ( "stapAchteruit" ) );
		gameButtons.getChildren ().add ( btnDraaiLinks = new Button ( "draaiLinks" ) );
		gameButtons.getChildren ().add ( btnDraaiRechts = new Button ( "draaiRechts" ) );
		gameButtons.getChildren ().add ( boxTypes = new ComboBox <> () );
		gameButtons.getChildren ().addAll ( btnCosts = new Button ( "Kosten" ) );
		boxTypes.getItems ().setAll ( ScreenBoxType.CACHE );
		boxTypes.getSelectionModel ().select ( 0 );

		final HBox gladeButtons = createHBox ();
		Button     btnLoad, btnSave;
		gladeButtons.getChildren ().add ( btnLoad = new Button ( "Load" ) );
		gladeButtons.getChildren ().add ( btnSave = new Button ( "Save" ) );

		btnCode.setDisable ( true );
		btnRun.setDisable ( true );

		btnCode.setOnAction ( this::onCode );
		btnStart.setOnAction ( this::onStart );
		btnRun.setOnAction ( this::onRun );
		btnReset.setOnAction ( this::onReset );
		btnDebug.setOnAction ( this::onDebug );
		btnCosts.setOnAction ( this::onCosts );
		btnStapVooruit.setOnAction ( this::onStapVooruit );
		btnStapAchteruit.setOnAction ( this::onStapAchteruit );
		btnDraaiLinks.setOnAction ( this::onDraaiLinks );
		btnDraaiRechts.setOnAction ( this::onDraaiRechts );
		btnLoad.setOnAction ( this::onLoad );
		btnSave.setOnAction ( this::onSave );

		game.setTop ( gameButtons );
		game.setCenter ( maze );
		game.setBottom ( gladeButtons );

		root.setTop ( sysButtons );
		root.setCenter ( welcomeText );
		stage.setScene ( new Scene ( root, MAZE_SIZE, 180 ) );

		stage.show ();
		stage.setY ( stage.getY () - 200 );
	}

	public static GameWindow getInstance () { return instance; }

	public static Image getIcon () { return icon; }

	public void redrawMaze () {

		graphics.setFill ( Color.WHITE );
		graphics.fillRect ( 0, 0, MAZE_SIZE, MAZE_SIZE );

		for ( int x = 0; x < MAZE_BOXES; x++ )
			for ( int y = 0; y < MAZE_BOXES; y++ ) {
				getBoxAt ( x, y ).getType ().getDrawable ().onDraw ( graphics, boxes[ x ][ y ] );
				getObjectAt ( x, y ).getType ().getDrawable ().onDraw ( graphics, objects[ x ][ y ] );
			}
		poppetje.draw ( graphics );

		{ // Grid
			graphics.setStroke ( Color.BLACK );
			graphics.setLineWidth ( 0.5 );
			for ( int h = 0; h <= MAZE_SIZE; h += MAZE_BOX_WIDTH ) {
				graphics.strokeLine ( h, 0, h, MAZE_SIZE );
				graphics.strokeLine ( 0, h, MAZE_SIZE, h );
			}
		}
	}

	public Poppetje getPoppetje () { return poppetje; }

	public ScreenBox getBoxAt ( int x, int y ) {

		if ( 0 > x | x >= MAZE_BOXES ) throw new IllegalArgumentException ( "x must be between 0 and " + MAZE_BOXES );
		else if ( 0 > y | y >= MAZE_BOXES ) throw new IllegalArgumentException ( "y must be between 0 and " + MAZE_BOXES );
		else {
			if ( boxes[ x ][ y ] == null ) boxes[ x ][ y ] = new ScreenBox ( x, y );
			return boxes[ x ][ y ];
		}
	}

	public ScreenObject getObjectAt ( int x, int y ) {

		if ( 0 > x | x >= MAZE_BOXES ) throw new IllegalArgumentException ( "x must be between 0 and " + MAZE_BOXES );
		else if ( 0 > y | y >= MAZE_BOXES ) throw new IllegalArgumentException ( "y must be between 0 and " + MAZE_BOXES );
		else {
			if ( objects[ x ][ y ] == null ) objects[ x ][ y ] = new ScreenObject ( x, y );
			return objects[ x ][ y ];
		}
	}

	private HBox createHBox () {

		HBox box = new HBox ();
		box.setAlignment ( BASELINE_CENTER );
		box.setPadding ( new Insets ( SPACING ) );
		box.setSpacing ( SPACING );
		return box;
	}

	private VBox createVBox () {

		VBox box = new VBox ();
		box.setAlignment ( BASELINE_CENTER );
		box.setPadding ( new Insets ( SPACING ) );
		box.setSpacing ( SPACING );
		return box;
	}

	private void onMazeEdit ( final MouseEvent event ) {

		int x = ( int ) Math.floor ( event.getX () / MAZE_BOX_WIDTH );
		int y = ( int ) Math.floor ( event.getY () / MAZE_BOX_WIDTH );

		if ( poppetje.getX () == x & poppetje.getY () == y ) poppetje.reset ();
		else switch ( event.getButton () ) {
			case PRIMARY -> {
				if ( event.isShiftDown () ) SettingsWindow.getInstance ().show ( x, y );
				else getBoxAt ( x, y ).setTypeNext ();
			}
			case SECONDARY -> getBoxAt ( x, y ).setType ( boxTypes.getSelectionModel ().getSelectedItem () );
			default -> {}
		}
		redrawMaze ();
	}

	private void onCode ( final ActionEvent event ) { CodeWindow.getInstance ().show (); }

	private void onStart ( final ActionEvent event ) {

		sysButtons.getChildren ().remove ( btnStart );
		sysButtons.getChildren ().add ( 2, btnReset );
		btnCode.setDisable ( false );
		btnRun.setDisable ( false );
		root.setCenter ( game );
		stage.setHeight ( MAZE_SIZE + 160 );

		redrawMaze ();
	}

	private void onRun ( final ActionEvent event ) { Main.getInstance ().run (); }

	private void onReset ( final ActionEvent event ) {

		for ( int x = 0; x < MAZE_BOXES; x++ )
			for ( int y = 0; y < MAZE_BOXES; y++ ) {
				getBoxAt ( x, y ).setType ( ScreenBoxType.COLOR_WHITE );
				getObjectAt ( x, y ).setType ( ScreenObjectType.NONE );
			}
		poppetje.reset ();
		redrawMaze ();
	}

	private void onDebug ( final ActionEvent event ) { btnDebug.setText ( Main.getInstance ().toggleDebug () ? "Disable Debug" : "Enable Debug" ); }

	private void onCosts ( final ActionEvent event ) { CostWindow.getInstance ().show (); }

	private void onStapVooruit ( final ActionEvent event ) { poppetje.moveForward (); }

	private void onStapAchteruit ( final ActionEvent event ) { poppetje.moveBackwards (); }

	private void onDraaiLinks ( final ActionEvent event ) { poppetje.turnLeft (); }

	private void onDraaiRechts ( final ActionEvent event ) { poppetje.turnRight (); }

	private void onLoad ( final ActionEvent event ) {

		try {
			ObjectInputStream reader = new ObjectInputStream ( new FileInputStream ( Main.GLADE_FILE ) );
			boxes = ( ScreenBox[][] ) reader.readObject ();
			objects = ( ScreenObject[][] ) reader.readObject ();
			reader.close ();
		} catch ( IOException | ClassNotFoundException e ) { Main.showError ( e ); }
		redrawMaze ();
	}

	private void onSave ( final ActionEvent event ) {

		try {
			if ( !Main.GLADE_FILE.exists () ) if ( !Main.GLADE_FILE.createNewFile () ) Main.log ( "No new glade file created!" );
			ObjectOutputStream writer = new ObjectOutputStream ( new FileOutputStream ( Main.GLADE_FILE ) );
			writer.writeObject ( boxes );
			writer.writeObject ( objects );
			writer.close ();
		} catch ( IOException e ) { Main.showError ( e ); }
	}
}
