package io.switchware.swaggerjagger.ui;

import io.switchware.swaggerjagger.Main;
import io.switchware.swaggerjagger.ui.objects.ScreenBoxType;
import io.switchware.swaggerjagger.ui.objects.ScreenObject;
import io.switchware.swaggerjagger.ui.objects.ScreenObjectType;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import static io.switchware.swaggerjagger.Main.SPACING;

public final class SettingsWindow {

	private static final SettingsWindow INSTANCE = new SettingsWindow ();

	private final Stage                         stage;
	private final ComboBox < ScreenBoxType >    comboBoxType;
	private final ComboBox < ScreenObjectType > comboObjectType;
	private final TextField                     value;

	private int x, y;

	private SettingsWindow () {

		stage = new Stage ();
		stage.setTitle ( "Tile Editor" );
		stage.getIcons ().setAll ( GameWindow.getIcon () );
		stage.initStyle ( StageStyle.UTILITY );

		BorderPane root = new BorderPane ();
		GridPane   grid = new GridPane ();
		grid.setPadding ( new Insets ( SPACING ) );
		grid.setHgap ( SPACING );
		grid.setVgap ( SPACING );

		grid.add ( new Label ( "Kleur" ), 0, 0 );
		grid.add ( new Label ( "Object" ), 0, 1 );
		grid.add ( new Label ( "Waarde" ), 0, 2 );
		grid.add ( comboBoxType = new ComboBox <> (), 1, 0 );
		grid.add ( comboObjectType = new ComboBox <> (), 1, 1 );
		grid.add ( value = new TextField (), 1, 2 );

		comboBoxType.getItems ().setAll ( ScreenBoxType.CACHE );
		comboObjectType.getItems ().setAll ( ScreenObjectType.CACHE );
		comboBoxType.setOnAction ( this::onBoxChange );
		comboObjectType.setOnAction ( this::onObjectChange );
		value.setOnAction ( this::onValue );

		root.setCenter ( grid );
		stage.setScene ( new Scene ( root, 250, 140 ) );
		stage.setResizable ( false );
	}

	public static SettingsWindow getInstance () { return INSTANCE; }

	public void show ( final int x, final int y ) {

		stage.setTitle ( String.format ( "Tile Editor > %d, %d", this.x = x, this.y = y ) );

		final ScreenObject obj = GameWindow.getInstance ().getObjectAt ( x, y );
		comboBoxType.getSelectionModel ().select ( GameWindow.getInstance ().getBoxAt ( x, y ).getType () );
		comboObjectType.getSelectionModel ().select ( obj.getType () );

		value.setText ( String.valueOf ( obj.getValue () ) );
		stage.show ();
		stage.requestFocus ();
	}

	private void onBoxChange ( final ActionEvent event ) {

		GameWindow.getInstance ().getBoxAt ( x, y ).setType ( comboBoxType.getSelectionModel ().getSelectedItem () );
		GameWindow.getInstance ().redrawMaze ();
	}

	private void onObjectChange ( final ActionEvent event ) {

		GameWindow.getInstance ().getObjectAt ( x, y ).setType ( comboObjectType.getSelectionModel ().getSelectedItem () );
		GameWindow.getInstance ().redrawMaze ();
	}

	private void onValue ( final ActionEvent event ) {

		try {
			GameWindow.getInstance ().getObjectAt ( x, y ).setValue ( Integer.parseInt ( value.getText () ) );
		} catch ( final NumberFormatException e ) {
			Main.showError ( e );
		}
		GameWindow.getInstance ().redrawMaze ();
	}
}
