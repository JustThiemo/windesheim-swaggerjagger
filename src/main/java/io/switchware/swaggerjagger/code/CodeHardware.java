package io.switchware.swaggerjagger.code;

public enum CodeHardware {
	COMPASS,
	COLOR_EYE,
	BW_EYE
}
