package io.switchware.swaggerjagger.ui.objects;

import io.switchware.swaggerjagger.ui.draw.Drawable;
import io.switchware.swaggerjagger.ui.draw.ImageDrawable;
import io.switchware.swaggerjagger.ui.draw.NullDrawable;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import lombok.Getter;

public enum ScreenObjectType {

	NONE ( new NullDrawable (), null ),
	OBSTACLE ( new ImageDrawable ( "/steen.png" ), Color.WHITE ),
	BOMB ( new ImageDrawable ( "/bom.png" ), Color.BLACK ),
	TURN ( new ImageDrawable ( "/draai.png" ), Color.BLUE ),
	TREE ( new ImageDrawable ( "/boom.png" ), Color.WHITE ),
	BUSH ( new ImageDrawable ( "/struikje.png" ), Color.WHITE ),
	TARGET ( new ImageDrawable ( "/target.png" ), Color.YELLOW ),
	BONUS ( new ImageDrawable ( "/stackies.png" ), Color.WHITE );

	public static final   ScreenObjectType[] CACHE = values ();
	@Getter private final Drawable           drawable;
	@Getter private final Paint              background;

	ScreenObjectType ( final Drawable drawable, final Paint background ) {

		this.drawable = drawable;
		this.background = background;
	}

	public static ScreenObjectType fromId ( int id ) {

		for ( ScreenObjectType type : CACHE ) if ( type.ordinal () == id ) return type;
		throw new IllegalArgumentException ( "Invalid ScreenObjectType ID " + id );
	}

	public static ScreenObjectType getNext ( ScreenObjectType current ) { return fromId ( ( current.ordinal () + 1 ) % CACHE.length ); }
}
