package io.switchware.swaggerjagger.keyword;

import io.switchware.swaggerjagger.code.CodeBlock;

public abstract class KeyWordHandler {

	/**
	 * Execute a statement.
	 *
	 * @param lineIndex
	 * 	the index of the line it started on
	 * @param args
	 * 	the arguments given after the statement
	 * @param block
	 * 	the code block which can be null
	 *
	 * @throws KeyWordException
	 * 	When invalid arguments where given.
	 */
	public abstract void onHandle ( int lineIndex, String args, CodeBlock block ) throws KeyWordException;

	/**
	 * @return determine if a keyword contains a code block or just a single line
	 */
	public abstract boolean isBlock ();

	protected void waitPls () {
		// Wait 500ms to do animations and shit yah feel me.
		try {
			Thread.sleep ( 200 );
		} catch ( InterruptedException e ) {
			e.printStackTrace ();
		}
	}
}
