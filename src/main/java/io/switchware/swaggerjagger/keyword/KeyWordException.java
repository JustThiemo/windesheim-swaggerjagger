package io.switchware.swaggerjagger.keyword;

public class KeyWordException extends Exception {

	public KeyWordException ( String message ) { super ( message ); }

	public KeyWordException ( String message, Throwable cause ) { super ( message, cause ); }

	public KeyWordException ( Throwable cause ) { super ( cause ); }
}
