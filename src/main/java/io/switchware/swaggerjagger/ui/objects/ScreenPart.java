package io.switchware.swaggerjagger.ui.objects;

import java.io.Serializable;

public abstract class ScreenPart < T > implements Serializable {

	T type;
	final int x, y;

	ScreenPart ( final T type, final int x, final int y ) {

		this.type = type;
		this.x = x;
		this.y = y;
	}

	@Override public String toString () { return String.format ( "%s[x=%d,y=%d,type=%s]", getClass ().getSimpleName (), x, y, type ); }

	public final T getType () { return type; }

	public final void setType ( final T type ) { this.type = type; }

	public abstract void setTypeNext ();

	public final int getX () { return x; }

	public final int getY () { return y; }
}
