package io.switchware.swaggerjagger.ui.draw;

import io.switchware.swaggerjagger.ui.objects.ScreenPart;
import javafx.scene.canvas.GraphicsContext;

public class NullDrawable implements Drawable {

	@Override public void onDraw ( final GraphicsContext graphics, final ScreenPart box ) {}
}
