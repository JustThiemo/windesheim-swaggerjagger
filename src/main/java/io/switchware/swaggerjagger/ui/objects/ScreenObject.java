package io.switchware.swaggerjagger.ui.objects;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

public class ScreenObject extends ScreenPart < ScreenObjectType > {

	@Getter @Setter private int value = 0;

	public ScreenObject ( int x, int y ) { super ( ScreenObjectType.NONE, x, y ); }

	@Override public boolean equals ( Object obj ) { return obj instanceof ScreenObject && ( x == ( ( ScreenObject ) obj ).x && y == ( ( ScreenObject ) obj ).y ); }

	@Override public void setTypeNext () { this.type = ScreenObjectType.getNext ( this.type ); }
}
