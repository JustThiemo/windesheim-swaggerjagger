package io.switchware.swaggerjagger.code;

import io.switchware.swaggerjagger.Main;
import io.switchware.swaggerjagger.balance.CostHandler;
import io.switchware.swaggerjagger.balance.CostType;
import io.switchware.swaggerjagger.keyword.KeyWordException;
import io.switchware.swaggerjagger.keyword.KeyWordHandler;
import io.switchware.swaggerjagger.keyword.KeyWords;
import io.switchware.swaggerjagger.keyword.types.*;
import io.switchware.swaggerjagger.ui.CodeWindow;
import io.switchware.swaggerjagger.ui.GameWindow;
import io.switchware.swaggerjagger.ui.objects.ScreenObject;
import io.switchware.swaggerjagger.ui.objects.ScreenObjectType;
import lombok.AccessLevel;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter public class CodeExecutor {

	@Getter ( value = AccessLevel.NONE ) private final KeyWords                       keyWords;
	private final                                      HashMap < Character, Integer > assignedVariables = new HashMap <> ();
	private final                                      List < CodeHardware >          usedHardeware     = new ArrayList <> ();
	private final                                      Map < Integer, CodeBlock >     codeBlocks        = new HashMap <> ();
	// Keep track of current code blocks.
	private                                            int                            currentBlock      = 0;
	// Keep track of full code.
	private                                            List < String >                fullCode          = new ArrayList <> ();
	private                                            boolean                        canUse            = true;
	private                                            int                            maxTarget         = 0, currentTarget = 0;

	public CodeExecutor ( KeyWords keyWords ) {

		this.keyWords = keyWords;
	}

	public void clear () {

		canUse = true;
		maxTarget = 0;
		currentTarget = 0;
		ZolangKeyHandler.FORCE_STOP = false;
		usedHardeware.clear ();
		assignedVariables.clear ();
	}

	public void addHardware ( CodeHardware hardware ) throws KeyWordException {

		if ( hasHardware ( hardware ) ) throw new KeyWordException ( "Already using " + hardware.name () + " (yes just translate this to Dutch and it will make sense)" );
		usedHardeware.add ( hardware );
	}

	public boolean hasHardware ( CodeHardware hardware ) {

		return usedHardeware.contains ( hardware );
	}

	/**
	 * Executes all of the loaded lines of code.
	 *
	 * @param lines
	 * 	the loaded code lines
	 * @param currentLine
	 * 	the line it's currently reading.
	 *
	 * @throws KeyWordException
	 * 	when your code is invalid
	 */
	private void execute ( List < String > lines, int currentLine ) throws KeyWordException {

		int maxLines = lines.size ();
		int onLine   = currentLine;
		if ( currentTarget == maxTarget ) {
			ZolangKeyHandler.FORCE_STOP = true;
			return;
		}

		if ( currentLine < maxLines ) {
			String   line  = lines.get ( currentLine );
			String[] parts = line.trim ().split ( " " );

			if ( keyWords.isKeyWord ( parts[ 0 ] ) ) {
				// It is an keyboard which can be executed to do something
				KeyWordHandler handler = keyWords.getHandlers ().get ( parts[ 0 ].toLowerCase () );
				if ( !( handler instanceof GebruikKeyHandler ) ) canUse = false;

				// Check if its a block of code, a block of code needs
				if ( !handler.isBlock () ) {
					executeSingleLine ( onLine, line );

					// Go to the next line, and call this method again to execute the next one.
					onLine++;
					execute ( lines, onLine );
				} else {
					CodeBlock block = codeBlocks.get ( onLine );
					//Main.log ( Arrays.toString ( parts ) );
					handler.onHandle ( onLine, keyWords.parseKeyWord ( line.toLowerCase () ), block );
					// Thread continues after the full block has been executed, so we can continue after this block.
					execute ( lines, block.getEndLine () );
				}
			} else {
				executeSingleLine ( onLine, line );
				execute ( lines, onLine + 1 ); // Execute next line.
			}
		}
	}

	public boolean canUse () { return canUse; }

	public void preAnalyze ( List < String > lines ) throws KeyWordException {

		List < CodeBlock > blocks = new ArrayList <> ();
		for ( String line : lines ) {
			String[] parts = line.trim ().split ( " " );

			if ( keyWords.isKeyWord ( parts[ 0 ] ) ) {
				KeyWordHandler handler = keyWords.getHandlers ().get ( parts[ 0 ].toLowerCase () );
				if ( handler.isBlock () ) {
					// Only save blocks of code.
					int             startFrom    = lines.indexOf ( line ), endAt = -1;
					List < String > executeLines = new ArrayList <> ();
					int             shouldEnd    = 0;

					for ( int b = ( startFrom + 1 ); b < lines.size (); b++ ) {
						String insideLine = lines.get ( b );
						insideLine = insideLine.trim ();

						if ( insideLine.contains ( "{" ) ) shouldEnd++;
						executeLines.add ( insideLine );

						if ( insideLine.equalsIgnoreCase ( "}" ) ) {
							if ( shouldEnd == 0 ) {
								endAt = b;
								break;
							} else shouldEnd--;
						}
					}

					if ( endAt == -1 ) throw new KeyWordException ( "Code block did not have an ending." );

					CodeBlock block = new CodeBlock ( startFrom, endAt );
					block.getLinesInside ().addAll ( executeLines );
					blocks.add ( block );
				}
			}
		}

		ScreenObject[][] objects = GameWindow.getInstance ().getObjects ();
		for ( int x = 0; x < Main.MAZE_BOXES; x++ )
			for ( int y = 0; y < Main.MAZE_BOXES; y++ ) {
				ScreenObject obj = objects[ x ][ y ];
				if ( obj.getType ().equals ( ScreenObjectType.TARGET ) && obj.getValue () > maxTarget ) maxTarget = obj.getValue ();
			}

		for ( CodeBlock block : blocks ) {
			for ( String s : block.getLinesInside () ) Main.log ( s );
			codeBlocks.put ( block.getStartLine (), block );
		}

		fullCode = lines;
		analyzeCosts ();

		execute ( lines, 0 );

		// Print out final balance.
		System.out.println ( "Total cost for your declarations (lines of code): " + CostHandler.getInstance ().getTotalDeclareCost () );
		System.out.println ( "Balance: " + CostHandler.getInstance ().getBalance () );
		if ( CostHandler.getInstance ().getBalance () < 0 ) throw new KeyWordException ( "You ran out of balance: " + CostHandler.getInstance ().getBalance () );

		Main.log ( "Done executing program" );
	}

	public void offerTarget ( final int target ) { currentTarget = target; }

	private void analyzeCosts () {

		for ( String line : fullCode ) {
			if ( line.isBlank () ) continue;

			String[] parts = line.trim ().split ( " " );
			if ( keyWords.isKeyWord ( parts[ 0 ] ) ) {
				KeyWordHandler handler = keyWords.getHandlers ().get ( parts[ 0 ].toLowerCase () );
				if ( handler instanceof GebruikKeyHandler ) continue;

				if ( handler instanceof ZolangKeyHandler ) CostHandler.getInstance ().takeCosts ( CostType.DECLARE_WHILE );

				if ( handler instanceof AlsKeyHandler ) CostHandler.getInstance ().takeCosts ( CostType.DECLARE_IF );

				if ( handler instanceof DraaiRechtsHandler || handler instanceof DraaiLinksHandler || handler instanceof StapAchteruitHandler || handler instanceof StapVooruitHandler ) {
					CostHandler.getInstance ().takeCosts ( CostType.DECLARE_ASSISNGMENT );
				}
			} else {
				if ( keyWords.isChar ( parts[ 0 ] ) && parts[ 1 ].equalsIgnoreCase ( "=" ) ) {
					CostHandler.getInstance ().takeCosts ( CostType.DECLARE_EXERCISE );
				}
			}
		}
	}

	/**
	 * Execute a single line of code
	 *
	 * @param lineIndex
	 * 	the line index
	 * @param line
	 * 	the line you want to execute
	 *
	 * @throws KeyWordException
	 * 	when your code fails
	 */
	private void executeSingleLine ( int lineIndex, String line ) throws KeyWordException {

		// Ignore empty lines or lines that are connected to a code block.
		if ( line.isBlank () || line.trim ().equalsIgnoreCase ( "{" ) || line.trim ().equalsIgnoreCase ( "}" ) ) return;

		line = line.trim ();
		String[] parts = line.split ( " " );
		Main.log ( "Doing line: " + line );

		if ( keyWords.isKeyWord ( parts[ 0 ] ) ) {
			// This code is a keyword, let's see what keyword it is and execute it.
			KeyWordHandler handler = keyWords.getHandlers ().get ( parts[ 0 ].toLowerCase () );
			// Make sure this is not a code block, we can not accept code blocks as a single line.
			if ( !handler.isBlock () ) try {
				handler.onHandle ( lineIndex, keyWords.parseKeyWord ( line ), null );
			} catch ( KeyWordException e ) {
				e.printStackTrace ();
			}
		} else {
			// Lines like a = c + 3, or any line that doesn't start with a registered keyword.
			if ( keyWords.isChar ( parts[ 0 ] ) && parts[ 0 ].length () == 1 ) {
				char c = parts[ 0 ].charAt ( 0 );
				if ( isVariableAssigned ( c ) ) {
					if ( parts.length == 3 ) {
						if ( isNumber ( parts[ 2 ] ) ) assignedVariables.put ( c, Integer.parseInt ( parts[ 2 ] ) );
						else {
							char target = parts[ 2 ].charAt ( 0 );
							if ( isVariableAssigned ( target ) ) throw new KeyWordException ( "Can not do anything with unassigned character " + target );
							int amount = assignedVariables.get ( target );
							assignedVariables.put ( c, amount );
						}
						return;
					}

					int mathThings = JsEngine.evalInt ( line.split ( " =" )[ 1 ] );
					assignedVariables.put ( c, mathThings );
				} else throw new KeyWordException ( "Variable " + c + " is not assigned and can not be used." );
			} else throw new KeyWordException ( "Found bullshit code on line " + line );
		}
	}

	public int getIntValue ( String input ) throws KeyWordException {

		if ( isNumber ( input ) ) return Integer.parseInt ( input );
		else {
			char target = input.charAt ( 0 );
			if ( !isVariableAssigned ( target ) ) throw new KeyWordException ( "Can not do anything with unassigned character " + target );
			return assignedVariables.get ( target );
		}
	}

	private boolean isNumber ( String n ) {

		try {
			Integer.parseInt ( n );
			return true;
		} catch ( NumberFormatException e ) {
			return false;
		}
	}

	public boolean isVariableAssigned ( Character character ) {

		return assignedVariables.containsKey ( character );
	}

	public void assignVariable ( char c ) {

		assignedVariables.put ( c, 0 );
	}

	public KeyWords getKeyWordHandler () {

		return keyWords;
	}

	public void executeCodeBlock ( CodeBlock block ) throws KeyWordException {

		List < String > lines = new ArrayList <> ();
		for ( int i = 0; i < block.getEndLine (); i++ ) lines.add ( fullCode.get ( i ) );
		execute ( lines, ( block.getStartLine () + 1 ) );
	}
}
