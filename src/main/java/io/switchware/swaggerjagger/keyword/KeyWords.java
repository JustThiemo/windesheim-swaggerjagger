package io.switchware.swaggerjagger.keyword;

import io.switchware.swaggerjagger.Main;
import io.switchware.swaggerjagger.keyword.types.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class KeyWords {

	private final char[]                             variables = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
	private final HashMap < String, KeyWordHandler > handlers  = new HashMap <> ();

	public void load () {

		handlers.put ( "zolang", new ZolangKeyHandler () );
		handlers.put ( "gebruik", new GebruikKeyHandler () );
		handlers.put ( "stapvooruit", new StapVooruitHandler () );
		handlers.put ( "stapachteruit", new StapAchteruitHandler () );
		handlers.put ( "draairechts", new DraaiRechtsHandler () );
		handlers.put ( "draailinks", new DraaiLinksHandler () );
		handlers.put ( "als", new AlsKeyHandler () );
	}

	public boolean isKeyWord ( String word ) { return handlers.containsKey ( word.toLowerCase () ); }

	public String parseKeyWord ( String line ) {

		line = line.trim ();
		String[] parts = line.split ( " " );
		if ( isKeyWord ( parts[ 0 ] ) ) {
			StringBuilder args = new StringBuilder ();
			for ( int i = 1; i < parts.length; i++ ) args.append ( parts[ i ] ).append ( " " );
			return args.length () > 1 ? args.toString ().substring ( 0, args.length () - 1 ) : args.toString ();
		}
		return null;
	}

	public boolean isChar ( String ch ) {

		List < Character > chars = new ArrayList <> ();
		for ( char c : Main.getInstance ().getCodeExecutor ().getKeyWordHandler ().getVariables () ) chars.add ( c );
		return chars.contains ( ch.charAt ( 0 ) );
	}

	public char[] getVariables () { return variables; }

	public HashMap < String, KeyWordHandler > getHandlers () { return handlers; }
}
