package io.switchware.swaggerjagger.keyword.types;

import io.switchware.swaggerjagger.Main;
import io.switchware.swaggerjagger.balance.CostHandler;
import io.switchware.swaggerjagger.balance.CostType;
import io.switchware.swaggerjagger.code.CodeBlock;
import io.switchware.swaggerjagger.code.JsEngine;
import io.switchware.swaggerjagger.keyword.KeyWordException;
import io.switchware.swaggerjagger.keyword.KeyWordHandler;

public class AlsKeyHandler extends KeyWordHandler {

	@Override public void onHandle ( int lineIndex, String args, CodeBlock block ) throws KeyWordException {

		if ( args.isEmpty () ) throw new KeyWordException ( "Empty als statement" );

		String  line    = args.trim ().replace ( "{", "" );
		boolean execute = JsEngine.eval ( line );
		if ( execute ) Main.getInstance ().getCodeExecutor ().executeCodeBlock ( block );
	}

	@Override public boolean isBlock () { return true; }
}
